import { useEffect, useRef } from 'react';

import { Demo } from '@/demo/Demo.04';

export default function DemoPage() {
  const contaienr = useRef<HTMLDivElement>(null);

  useEffect(() => {
    console.log(contaienr);
    Demo.init(contaienr.current);
  });

  return <div className="page-container" ref={contaienr}></div>;
}
