import { NavLink } from 'umi';
import routes from '@/config/routes';

export default function IndexPage() {
  return (
    <div>
      <h1>Home Page </h1>
      <ul>
        {routes.map((route, index) => (
          <li key={index}>
            <NavLink to={route.path}>{route.component}</NavLink>
          </li>
        ))}
      </ul>
    </div>
  );
}
