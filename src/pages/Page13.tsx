import { useEffect, useRef, useState } from 'react';

import { Demo } from '@/demo/Demo.13';

export default function DemoPage() {
  const contaienr = useRef<HTMLDivElement>(null);
  const demoRef = useRef<Demo>(null);
  const [modelBlobMap, setModelBlobMap] = useState({});

  useEffect(() => {
    const demo = Demo.init(contaienr.current);
    demoRef.current = demo;
  }, []);

  useEffect(() => {
    if (!modelBlobMap || !Object.keys(modelBlobMap).length) {
      return;
    }
    console.log('useEffect-', modelBlobMap);

    demoRef.current.setModelBlobMap(modelBlobMap);
  }, [modelBlobMap]);

  return (
    <div
      className="page-container"
      ref={contaienr}
      onDrop={(event) => {
        event.preventDefault();

        if (event.type !== 'drop') {
          return;
        }
        const files = event.dataTransfer.files;
        const file = files[0];

        const newModelBlobMap = {
          ...modelBlobMap,
          [file.name]: file,
        };
        setModelBlobMap(newModelBlobMap);

        // demoRef.current.setLoadingManager(newModelBlobMap);
      }}
      onDragEnter={(event) => {
        event.preventDefault();
      }}
      onDragOver={(event) => {
        event.preventDefault();
      }}
    ></div>
  );
}
