import * as THREE from 'three';
import * as dat from 'dat.gui';

import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

export class Demo {
  public player: number = null;
  public scene: THREE.Scene = null;
  public camera: THREE.Camera = null;
  public renderer: THREE.Renderer = null;

  public gltfModel: GLTF = null;
  public model: THREE.Object3D = null;
  public mixer: THREE.AnimationMixer = null;

  // https://github.com/mrdoob/three.js/blob/dev/src/core/Clock.js
  public clock = new THREE.Clock();
  public animateActions: THREE.AnimationAction[] = [];

  public gui: dat.GUI = null;

  public constructor(private container: HTMLElement) {
    (window as any).d = this;

    this.createScene()
      .createGeometry()
      .createControl()
      .loadModel()
      .then((gltfModel) => {
        console.log(gltfModel);

        const obj = gltfModel.scene;
        // obj.position.set(0, 0, 0);
        // obj.scale.set(1, 1, 1);

        this.model = obj;
        this.gltfModel = gltfModel;
        this.scene.add(obj);

        this.getAnimation();
        this.startPlay();
      })
      .catch(console.error);
  }

  public static init(container: HTMLElement): Demo {
    return new Demo(container);
  }

  public createScene() {
    const width = window.innerWidth; // 窗口宽度
    const height = window.innerHeight; // 窗口高度

    const scene = new THREE.Scene();
    const renderer = new THREE.WebGLRenderer({
      //增加下面两个属性，可以抗锯齿
      antialias: true,
      alpha: true,
    });

    const camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000);

    renderer.setPixelRatio(window.devicePixelRatio);
    camera.position.set(20, 30, 60);
    camera.lookAt(scene.position);

    this.scene = scene;
    this.camera = camera;
    this.renderer = renderer;

    renderer.setSize(width, height);
    renderer.setClearColor(0xb9d3ff, 1);
    this.container.appendChild(renderer.domElement);

    return this;
  }

  public createGeometry() {
    // ----------------------------- // Face3对象定义Geometry的三角形面
    /**
    !!! 新版没有 THREE.Geometry()

    设置Geometry顶点位置、顶点颜色数据
    var geometry = new THREE.Geometry(); //声明一个几何体对象Geometry
    var p1 = new THREE.Vector3(50, 0, 0); //顶点1坐标
    var p2 = new THREE.Vector3(0, 70, 0); //顶点2坐标
    var p3 = new THREE.Vector3(80, 70, 0); //顶点3坐标
    // 顶点坐标添加到 geometry 对象
    geometry.vertices.push(p1, p2, p3);

    geometry.vertices -> BufferGeometry.attributes.position
    geometry.colors   -> BufferGeometry.attributes.color
     */

    /**
    ThreeJS 动画系统中的一些概念：
    - AnimationClip 动画片段 - 每个片段保存对象某个活动数据，模型中多个动画就有多个 AnimationClip
    - KeyframeTrack 关键帧轨道 - 在 AnimationClip 中，每个动画属性数据存储在一个单独的 KeyframeTrack 中，可以有很多 KeyframeTrack
    - AnimationMixer 动画混合器 - AnimationClip/KeyframeTrack 数据构成动画基础，实际播放由 AnimationMixer 控制，可以同时控制和混合若干动画
    - AnimationAction 动画行为动作 - AnimationActions 用来调度存储在 AnimationClips 中的动画，决定何时播放、暂停或停止其中一个混合器中的某个 AnimationClip
    - AnimationObjectGroup 动画对象组 - 一组对象接收一个共享动画状态 ？

    ThreeJS 中并非多有加载器都支持 AnimationClip 序列，支持动画的加载器
    - THREE.ObjectLoader
    - THREE.BVHLoader
    - THREE.ColladaLoader
    - THREE.FBXLoader
    - THREE.GLTFLoader
    - THREE.MMDLoader
    - THREE.SEA3DLoader
     */

    const dodecahed = new THREE.IcosahedronGeometry(5, 10);
    const material = new THREE.MeshLambertMaterial({
      color: 0xff0000,
      // wireframe: true,
      // transparent设置为true，开启透明，否则opacity不起作用
      transparent: true,
      // 设置材质透明度
      opacity: 0.8,
    });
    const mesh1 = new THREE.Mesh(dodecahed, material); //网格模型对象Mesh
    mesh1.position.set(10, 0, 0);
    this.scene.add(mesh1); //网格模型添加到场景中

    const point = new THREE.PointLight(0xffffff);
    point.position.set(50, 50, 50);
    this.scene.add(point);
    const ambient = new THREE.AmbientLight(0xdddddd);
    this.scene.add(ambient);

    // 辅助坐标系  参数250表示坐标系大小，可以根据场景大小去设置
    const axisHelper = new THREE.AxesHelper(300);
    const gridHelper = new THREE.GridHelper(100, 30, 0x2c2c2c, 0x888888);
    this.scene.add(axisHelper);
    this.scene.add(gridHelper);

    return this;
  }

  public loadModel() {
    // https://iaosee-resource.oss-cn-beijing.aliyuncs.com/model/Soldier.glb
    const loader = new GLTFLoader();

    return new Promise<GLTF>((resolve, reject) => {
      loader.load(
        // 'https://iaosee-resource.oss-cn-beijing.aliyuncs.com/model/Soldier.glb',
        // 'https://iaosee-resource.oss-cn-beijing.aliyuncs.com/model/Parrot.glb',
        'https://iaosee-resource.oss-cn-beijing.aliyuncs.com/model/RobotExpressive.glb',
        // 'https://workspace2427069.oss-cn-beijing.aliyuncs.com/%E6%A8%A1%E5%9E%8B%E4%B8%8A%E4%BC%A0%E5%88%B0%E8%BF%99%E9%87%8C/%E5%B7%A5%E4%B8%9A_%E5%AD%9D%E5%A8%81_2020.9.4/%E5%8C%96%E5%B7%A5_%E5%AD%9D%E5%A8%81_2020.10.10/banganshi/banganshi.glb',
        (model: GLTF) => {
          resolve(model);
        },
        undefined,
        (error) => {
          reject(error);
        },
      );
    });
  }

  public getAnimation() {
    const { gltfModel } = this;

    // 新建一个 AnimationMixer, 并取得 AnimationClip 实例列表
    const mixer = new THREE.AnimationMixer(gltfModel.scene);

    const animateActions: THREE.AnimationAction[] = []; // 存储所有动画 Action
    const guiControl: { [key: string]: Function } = {};
    const animationGroup = this.gui.addFolder('animations');
    this.animateActions = animateActions;

    for (let i = 0; i < gltfModel.animations.length; i++) {
      const clipItem = gltfModel.animations[i];
      animateActions[i] = mixer.clipAction(clipItem);
      // console.log(clipItem);
      guiControl[clipItem.name] = () => this.playAction(animateActions[i]);
      animationGroup.add(guiControl, clipItem.name);
    }

    this.mixer = mixer;

    console.log(mixer);
    // const clip = THREE.AnimationClip.findByName(gltfModel.animations, 'Run');
    // if ( clip ) {
    //   const action = this.mixer.clipAction(clip);
    //   action.play();
    // }

    return this;
  }

  public playAction(action: THREE.AnimationAction) {
    const { gltfModel, animateActions } = this;

    for (var i = 0; i < animateActions.length; i++) {
      if (action === animateActions[i]) {
        animateActions[i].play();
      } else {
        animateActions[i].stop();
      }
    }

    return this;
  }

  public createControl() {
    // 鼠标操作三维场景
    const controls = new OrbitControls(this.camera, this.renderer.domElement); //创建控件对象

    // 已经通过requestAnimationFrame(render);周期性执行render函数，没必要再通过监听鼠标事件执行render函数
    // controls.addEventListener('change', () => this.draw()); //监听鼠标、键盘事件
    // 注意开发中不要同时使用requestAnimationFrame()或controls.addEventListener('change', render)调用同一个函数，这样会冲突。

    // this.renderer.render(this.scene, this.camera);
    window.addEventListener('keyup', (e) => {
      if (e.code === 'Space') {
        controls.reset();
      }
    });

    const gui = new dat.GUI();
    this.gui = gui;

    return this;
  }

  public draw(t?: number) {
    //旋转角速度0.001弧度每毫秒
    // this.model.rotateY(0.001 * (t || 1));
    // this.model.rotateX(0.001 * (t || 1));

    this.mixer.update(this.clock.getDelta());

    this.renderer.render(this.scene, this.camera);
    return this;
  }

  public startPlay() {
    let T0 = Date.now(); // 上次时间
    const animate = (delta: number) => {
      // console.log(delta);
      // 均匀旋转
      const T1 = Date.now(); // 本次时间
      const t = T1 - T0;
      T0 = T1; // 把本次时间赋值给上次时间

      this.draw(delta);

      this.player = requestAnimationFrame(animate);
    };

    this.stopPlay();
    this.player = requestAnimationFrame(animate);
  }

  public stopPlay() {
    this.player && cancelAnimationFrame(this.player);
    return this;
  }
}
