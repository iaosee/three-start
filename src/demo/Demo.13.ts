import * as THREE from 'three';
import * as dat from 'dat.gui';

import { GLTF, GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

export class Demo {
  public player: number = null;
  public scene: THREE.Scene = null;
  public camera: THREE.Camera = null;
  public renderer: THREE.Renderer = null;

  public gltfModel: GLTF = null;
  public model: THREE.Object3D = null;
  public modelBlobMap: { [key: string]:  Blob } = {};

  // https://github.com/mrdoob/three.js/blob/dev/src/core/Clock.js
  public clock = new THREE.Clock();
  public animateActions: THREE.AnimationAction[] = [];
  public loadingManager = new THREE.LoadingManager();

  public config = {
    url: 'Parrot.glb',
    loadMoadel: () => this.loadMoadelHandler(),
  }
  public gui: dat.GUI = null;

  public constructor(private container: HTMLElement) {
    (window as any).d = this;

    this.createScene().createGeometry().createControl().startPlay();
  }

  public static init(container: HTMLElement): Demo {
    return new Demo(container);
  }

  public createScene() {
    const width = window.innerWidth; // 窗口宽度
    const height = window.innerHeight; // 窗口高度

    const scene = new THREE.Scene();
    const renderer = new THREE.WebGLRenderer({
      //增加下面两个属性，可以抗锯齿
      antialias: true,
      alpha: true,
    });

    const camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000);

    renderer.setPixelRatio(window.devicePixelRatio);
    camera.position.set(20, 30, 60);
    camera.lookAt(scene.position);

    this.scene = scene;
    this.camera = camera;
    this.renderer = renderer;

    renderer.setSize(width, height);
    renderer.setClearColor(0xb9d3ff, 1);
    this.container.appendChild(renderer.domElement);

    return this;
  }

  public createGeometry() {


    const point = new THREE.PointLight(0xffffff);
    point.position.set(50, 50, 50);
    this.scene.add(point);
    const ambient = new THREE.AmbientLight(0xdddddd);
    this.scene.add(ambient);


    // 辅助坐标系  参数250表示坐标系大小，可以根据场景大小去设置
    const axisHelper = new THREE.AxesHelper(300);
    const gridHelper = new THREE.GridHelper(100, 30, 0x2c2c2c, 0x888888);
    this.scene.add(axisHelper);
    this.scene.add(gridHelper);

    return this;
  }

  public loadMoadel(url: string) {
    console.log('loadMoadel');
    const loader = new GLTFLoader(this.loadingManager);

    return new Promise<{ model: THREE.Object3D; animations: THREE.AnimationClip[] }>((resolve, reject) => {
      loader.load(url, obj => {
        resolve({ model: obj.scene, animations: obj.animations });
      }, (e) => {
        // console.log('onProgress', e);
      }, (e) => {
        console.error(e);
        reject(e);
      })
    });
  }

  public setModelBlobMap(modelBlobMap: { [key: string]:  Blob }) {
    this.modelBlobMap = modelBlobMap;
  }

  public setLoadingManager(blobsMap?: { [key: string]: Blob }) {

    console.log(blobsMap);
    if (!blobsMap) {
      return;
    }

    this.loadingManager.setURLModifier((url) => {
      const blob = blobsMap[url];

      if (blob) {
        const objUrl = URL.createObjectURL(blobsMap[url]);
        // objectURLs.push(objUrl);
        console.log('createObjectURL', url, objUrl);
        return objUrl;
      }
      console.log('orogin - ', url);
      return url;
    });
  }

  public async loadMoadelHandler() {

    this.setLoadingManager(this.modelBlobMap);

    const result = await this.loadMoadel(this.config.url);
    const { model, animations } = result;

    this.scene.remove(this.model);
    this.scene.add(model);
    this.model = model;
  }

  public createControl() {
    // 鼠标操作三维场景
    const controls = new OrbitControls(this.camera, this.renderer.domElement); //创建控件对象

    // 已经通过requestAnimationFrame(render);周期性执行render函数，没必要再通过监听鼠标事件执行render函数
    // controls.addEventListener('change', () => this.draw()); //监听鼠标、键盘事件
    // 注意开发中不要同时使用requestAnimationFrame()或controls.addEventListener('change', render)调用同一个函数，这样会冲突。

    // this.renderer.render(this.scene, this.camera);

    window.addEventListener('keyup', (e) => {
      if (e.code === 'Space') {
        console.log(e);
        controls.reset();
      }
    });

    const gui = new dat.GUI();
    gui.add(this.config, 'url').onFinishChange((v: string) => {
      this.loadMoadelHandler();
    });
    gui.add(this.config, 'loadMoadel');
    this.gui = gui;

    return this;
  }

  public draw(t?: number) {
    //旋转角速度0.001弧度每毫秒
    // this.model.rotateY(0.001 * (t || 1));
    // this.model.rotateX(0.001 * (t || 1));

    this.renderer.render(this.scene, this.camera);
    return this;
  }

  public startPlay() {
    let T0 = Date.now(); // 上次时间
    const animate = (delta: number) => {
      // console.log(delta);
      // 均匀旋转
      const T1 = Date.now(); // 本次时间
      const t = T1 - T0;
      T0 = T1; // 把本次时间赋值给上次时间

      this.draw(delta);

      this.player = requestAnimationFrame(animate);
    };

    this.stopPlay();
    this.player = requestAnimationFrame(animate);
  }

  public stopPlay() {
    this.player && cancelAnimationFrame(this.player);
    return this;
  }
}
