import * as THREE from 'three';

import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

export class Demo {
  public player: number = null;
  public scene: THREE.Scene = null;
  public camera: THREE.Camera = null;
  public renderer: THREE.Renderer = null;

  public model: THREE.Object3D = null;

  public constructor(private container: HTMLElement) {
    this.createScene()
      .createGeometry()
      .createControl()
      // .draw()
      .startPlay();
  }

  public static init(container: HTMLElement): Demo {
    return new Demo(container);
  }

  public createScene() {
    const width = window.innerWidth; // 窗口宽度
    const height = window.innerHeight; // 窗口高度

    const scene = new THREE.Scene();
    const renderer = new THREE.WebGLRenderer({
      //增加下面两个属性，可以抗锯齿
      antialias: true,
      alpha: true,
    });


    const camera = new THREE.PerspectiveCamera(
      75,
      width / height,
      0.1,
      1000,
    );

    renderer.setPixelRatio(window.devicePixelRatio);
    camera.position.set(0, 0, 300);
    camera.lookAt(scene.position);

    this.scene = scene;
    this.camera = camera;
    this.renderer = renderer;

    renderer.setSize(width, height);
    renderer.setClearColor(0xb9d3ff, 1);
    this.container.appendChild(renderer.domElement);

    return this;
  }

  public createGeometry() {
    const vertices = new Float32Array([
      0, 0, 0, //顶点1坐标
      50, 0, 0, //顶点2坐标
      0, 100, 0, //顶点3坐标
      0, 0, 0, //顶点4坐标
      0, 0, 100, //顶点5坐标
      50, 0, 0, //顶点6坐标
    ]);
    const normals = new Float32Array([
      0, 0, 1, //顶点1法向量
      0, 0, 1, //顶点2法向量
      0, 0, 1, //顶点3法向量
      0, 1, 0, //顶点4法向量
      0, 1, 0, //顶点5法向量
      0, 1, 0, //顶点6法向量
    ]);

    const geometry = new THREE.BufferGeometry();
    const attribueVertex = new THREE.BufferAttribute(vertices, 3);
    const attribueNormal = new THREE.BufferAttribute(normals, 3);

    const material = new THREE.MeshLambertMaterial({
      color: 0x0000ff,
      side: THREE.DoubleSide
    });

    geometry.attributes.normal = attribueNormal;    // 没有法线显示不了光照效果
    geometry.attributes.position = attribueVertex;

    const obj = new THREE.Mesh(geometry, material);
    // const obj = new THREE.Line(geometry, material);  // 线染模式
    // const obj = new THREE.Points(geometry, material); // 点渲染模式


    // console.log(obj);
    this.scene.add(obj);
    this.model = obj;


    // ----------------------------- // 不使用顶点索引

    const rectangleGeometry = new THREE.BufferGeometry(); //声明一个空几何体对象
    //类型数组创建顶点位置position数据
    const rectangleVertices = new Float32Array([
      0, 0, 0, //顶点1坐标
      80, 0, 0, //顶点2坐标
      80, 80, 0, //顶点3坐标
      0, 0, 0, //顶点4坐标   和顶点1位置相同
      80, 80, 0, //顶点5坐标  和顶点3位置相同
      0, 80, 0, //顶点6坐标
    ]);
    const rectangleNormals = new Float32Array([
      0, 0, 1, //顶点1法向量
      0, 0, 1, //顶点2法向量
      0, 0, 1, //顶点3法向量
      0, 0, 1, //顶点4法向量
      0, 0, 1, //顶点5法向量
      0, 0, 1, //顶点6法向量
    ]);
    rectangleGeometry.attributes.position = new THREE.BufferAttribute(rectangleVertices, 3);
    rectangleGeometry.attributes.normal = new THREE.BufferAttribute(rectangleNormals, 3);
    const rectangleMesh = new THREE.Mesh(rectangleGeometry, new THREE.MeshLambertMaterial({
      color: 0x00ff00,
      side: THREE.DoubleSide
    }));
    rectangleMesh.position.set(100, 0, 0);
    this.scene.add(rectangleMesh);

    // ----------------------------- // 使用顶点索引
    const rectangle2Geometry = new THREE.BufferGeometry(); //声明一个空几何体对象
    //类型数组创建顶点位置position数据
    const rectangle2Vertices = new Float32Array([
      0, 0, 0, //顶点1坐标
      80, 0, 0, //顶点2坐标
      80, 80, 0, //顶点3坐标
      0, 80, 0, //顶点4坐标
    ]);
    const rectangle2Normals = new Float32Array([
      0, 0, 1, //顶点1法向量
      0, 0, 1, //顶点2法向量
      0, 0, 1, //顶点3法向量
      0, 0, 1, //顶点4法向量
    ]);
    // Uint16Array 类型数组 - 创建顶点索引数据
    const indexes = new Uint16Array([
      // 0对应第1个顶点位置数据、第1个顶点法向量数据
      // 1对应第2个顶点位置数据、第2个顶点法向量数据
      // 索引值3个为一组，表示一个三角形的3个顶点
      0, 1, 2,
      0, 2, 3,
    ]);


    // ----------------------------- // Face3对象定义Geometry的三角形面
/**
!!! 新版没有 THREE.Geometry()

设置Geometry顶点位置、顶点颜色数据
var geometry = new THREE.Geometry(); //声明一个几何体对象Geometry
var p1 = new THREE.Vector3(50, 0, 0); //顶点1坐标
var p2 = new THREE.Vector3(0, 70, 0); //顶点2坐标
var p3 = new THREE.Vector3(80, 70, 0); //顶点3坐标
// 顶点坐标添加到 geometry 对象
geometry.vertices.push(p1, p2, p3);

geometry.vertices -> BufferGeometry.attributes.position
geometry.colors   -> BufferGeometry.attributes.color
 */

    rectangle2Geometry.index = new THREE.BufferAttribute(indexes, 1); // 1个为一组
    rectangle2Geometry.attributes.normal = new THREE.BufferAttribute(rectangle2Normals, 3);
    rectangle2Geometry.attributes.position = new THREE.BufferAttribute(rectangle2Vertices, 3);
    const rectangle2Mesh = new THREE.Mesh(rectangle2Geometry, new THREE.MeshLambertMaterial({
      color: 0x00ff00,
      side: THREE.DoubleSide
    }));
    rectangle2Mesh.position.set(100, -100, 0);
    this.scene.add(rectangle2Mesh);


    const point = new THREE.PointLight(0xffffff);
    point.position.set(400, 200, 300);;
    this.scene.add(point);
    const ambient = new THREE.AmbientLight(0x444444);
    this.scene.add(ambient);

    // 辅助坐标系  参数250表示坐标系大小，可以根据场景大小去设置
    const axisHelper = new THREE.AxesHelper(300);
    this.scene.add(axisHelper);

    return this;
  }

  public createControl() {
    // 鼠标操作三维场景
    const controls = new OrbitControls(this.camera, this.renderer.domElement);//创建控件对象

    // 已经通过requestAnimationFrame(render);周期性执行render函数，没必要再通过监听鼠标事件执行render函数
    // controls.addEventListener('change', () => this.draw()); //监听鼠标、键盘事件
    // 注意开发中不要同时使用requestAnimationFrame()或controls.addEventListener('change', render)调用同一个函数，这样会冲突。

    // this.renderer.render(this.scene, this.camera);
    window.addEventListener('keyup', (e) => {
      if (e.code === 'Space') {
        controls.reset();
      }
    });
    return this;
  }

  public draw(t?: number) {

    //旋转角速度0.001弧度每毫秒
    // this.model.rotateY(0.001 * (t || 1));
    // this.model.rotateX(0.001 * (t || 1));

    this.renderer.render(this.scene, this.camera);
    return this;
  }

  public startPlay() {
    let T0 = Date.now();  // 上次时间
    const animate = () => {
      // 均匀旋转
      const T1 = Date.now();  // 本次时间
      const t = T1 - T0;
      T0 = T1;  // 把本次时间赋值给上次时间

      this.draw(t);

      this.player = requestAnimationFrame(animate);
    };
    this.stopPlay();
    animate();
  }

  public stopPlay() {
    this.player && cancelAnimationFrame(this.player);
    return this;
  }
}
