import * as THREE from 'three';

export class Demo {
  public player: number = null;
  public scene: THREE.Scene = null;
  public camera: THREE.Camera = null;
  public renderer: THREE.Renderer = null;

  public cube: THREE.Object3D = null;

  public constructor(private container: HTMLElement) {
    this.createScene().createGeometry().startPlay();
  }

  public static init(container: HTMLElement): Demo {
    return new Demo(container);
  }

  public createScene() {
    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera(
      75,
      window.innerWidth / window.innerHeight,
      0.1,
      1000,
    );
    const renderer = new THREE.WebGLRenderer();

    // 使用 WebGL2 渲染
    // const canvas = document.createElement( 'canvas' );
    // const context = canvas.getContext( 'webgl2' );
    // const renderer = new THREE.WebGLRenderer( { canvas: canvas, context: context } );

    this.scene = scene;
    this.camera = camera;
    this.renderer = renderer;
    this.camera.position.z = 5;

    renderer.setSize(window.innerWidth, window.innerHeight);
    this.container.appendChild(renderer.domElement);

    return this;
  }

  public createGeometry() {
    const geometry = new THREE.BoxGeometry(1, 1, 1);
    const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });

    const cube = new THREE.Mesh(geometry, material);
    this.scene.add(cube);

    this.cube = cube;
    return this;
  }

  public draw() {
    this.cube.rotation.x += 0.01;
    this.cube.rotation.y += 0.01;

    this.renderer.render(this.scene, this.camera);
    return this;
  }

  public startPlay() {
    const animate = () => {
      this.draw();
      this.player = requestAnimationFrame(animate);
    };
    this.stopPlay();
    animate();
  }

  public stopPlay() {
    this.player && cancelAnimationFrame(this.player);
    return this;
  }
}
