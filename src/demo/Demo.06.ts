import * as THREE from 'three';

import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

export class Demo {
  public player: number = null;
  public scene: THREE.Scene = null;
  public camera: THREE.Camera = null;
  public renderer: THREE.Renderer = null;

  public model: THREE.Object3D = null;

  public constructor(private container: HTMLElement) {
    this.createScene()
      .createGeometry()
      .createControl()
      // .draw()
      .startPlay();
  }

  public static init(container: HTMLElement): Demo {
    return new Demo(container);
  }

  public createScene() {
    const width = window.innerWidth; // 窗口宽度
    const height = window.innerHeight; // 窗口高度

    const scene = new THREE.Scene();
    const renderer = new THREE.WebGLRenderer({
      //增加下面两个属性，可以抗锯齿
      antialias:true,
      alpha:true,
    });


    const camera = new THREE.PerspectiveCamera(
      75,
      width / height,
      0.1,
      1000,
    );

    renderer.setPixelRatio(window.devicePixelRatio);
    camera.position.set(0, 0, 300);
    camera.lookAt(scene.position);

    this.scene = scene;
    this.camera = camera;
    this.renderer = renderer;

    renderer.setSize(width, height);
    renderer.setClearColor(0xb9d3ff, 1);
    this.container.appendChild(renderer.domElement);

    return this;
  }

  public createGeometry() {
    const vertices = new Float32Array([
      0, 0, 0, //顶点1坐标
      50, 0, 0, //顶点2坐标
      0, 100, 0, //顶点3坐标
      0, 0, 10, //顶点4坐标
      0, 0, 100, //顶点5坐标
      50, 0, 10, //顶点6坐标
    ]);
    const colors = new Float32Array([
      1, 0, 0, //顶点1颜色
      0, 1, 0, //顶点2颜色
      0, 0, 1, //顶点3颜色
      1, 1, 0, //顶点4颜色
      0, 1, 1, //顶点5颜色
      1, 0, 1, //顶点6颜色
    ]);

    const geometry = new THREE.BufferGeometry();
    const attribueVertex = new THREE.BufferAttribute(vertices, 3);
    const attribueColor = new THREE.BufferAttribute(colors, 3); // 3个为一组,表示一个顶点的颜色数据RGB

    const material = new THREE.PointsMaterial({
      // color: 0x0000ff,
      vertexColors: true, //以顶点颜色为准
      size: 10.0,
    });

    const obj = new THREE.Mesh(geometry, material);
    // const obj = new THREE.Line(geometry, material);  // 线染模式
    // const obj = new THREE.Points(geometry, material); // 点渲染模式

    geometry.attributes.color = attribueColor;
    geometry.attributes.position = attribueVertex;

    // console.log(obj);
    this.scene.add(obj);
    this.model = obj;

    const point = new THREE.PointLight(0xffffff);
    point.position.set(400, 200, 300); //点光源位置
    this.scene.add(point); // 点光源添加到场景中
    const ambient = new THREE.AmbientLight(0x444444);
    this.scene.add(ambient);

    // 辅助坐标系  参数250表示坐标系大小，可以根据场景大小去设置
    // const axisHelper = new THREE.AxesHelper(300);
    // this.scene.add(axisHelper);

    return this;
  }

  public createControl() {
    // 鼠标操作三维场景
    const controls = new OrbitControls(this.camera, this.renderer.domElement);//创建控件对象

    // 已经通过requestAnimationFrame(render);周期性执行render函数，没必要再通过监听鼠标事件执行render函数
    // controls.addEventListener('change', () => this.draw()); //监听鼠标、键盘事件
    // 注意开发中不要同时使用requestAnimationFrame()或controls.addEventListener('change', render)调用同一个函数，这样会冲突。

    // this.renderer.render(this.scene, this.camera);
    window.addEventListener('keyup', (e) => {
      if ( e.code === 'Space') {
        controls.reset();
      }
    });
    return this;
  }

  public draw(t?: number) {

    //旋转角速度0.001弧度每毫秒
    // this.model.rotateY(0.001 * (t || 1));
    // this.model.rotateX(0.001 * (t || 1));

    this.renderer.render(this.scene, this.camera);
    return this;
  }

  public startPlay() {
    let T0 = Date.now();  // 上次时间
    const animate = () => {
      // 均匀旋转
      const T1 = Date.now();  // 本次时间
      const t = T1 - T0;
      T0 = T1;  // 把本次时间赋值给上次时间

      this.draw(t);

      this.player = requestAnimationFrame(animate);
    };
    this.stopPlay();
    animate();
  }

  public stopPlay() {
    this.player && cancelAnimationFrame(this.player);
    return this;
  }
}
