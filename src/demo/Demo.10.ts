import * as THREE from 'three';
import * as dat from 'dat.gui';

import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

export class Demo {
  public player: number = null;
  public scene: THREE.Scene = null;
  public camera: THREE.Camera = null;
  public renderer: THREE.Renderer = null;

  public gltfModel: GLTF = null;
  public model: THREE.Object3D = null;

  // https://github.com/mrdoob/three.js/blob/dev/src/core/Clock.js
  public clock = new THREE.Clock();
  public animateActions: THREE.AnimationAction[] = [];

  public gui: dat.GUI = null;

  public constructor(private container: HTMLElement) {
    (window as any).d = this;

    this.createScene().createGeometry().createControl().startPlay();
  }

  public static init(container: HTMLElement): Demo {
    return new Demo(container);
  }

  public createScene() {
    const width = window.innerWidth; // 窗口宽度
    const height = window.innerHeight; // 窗口高度

    const scene = new THREE.Scene();
    const renderer = new THREE.WebGLRenderer({
      //增加下面两个属性，可以抗锯齿
      antialias: true,
      alpha: true,
    });

    const camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000);

    renderer.setPixelRatio(window.devicePixelRatio);
    camera.position.set(20, 30, 60);
    camera.lookAt(scene.position);

    this.scene = scene;
    this.camera = camera;
    this.renderer = renderer;

    renderer.setSize(width, height);
    renderer.setClearColor(0xb9d3ff, 1);
    this.container.appendChild(renderer.domElement);

    return this;
  }

  public createGeometry() {
    //创建两个网格模型mesh1、mesh2
    const geometry = new THREE.BoxGeometry(20, 20, 20);
    const material = new THREE.MeshLambertMaterial({ color: 0x0000ff });
    const group = new THREE.Group();
    const mesh1 = new THREE.Mesh(geometry, material);
    const mesh2 = new THREE.Mesh(geometry, material);
    mesh2.translateX(25);
    //把mesh1型插入到组group中，mesh1作为group的子对象
    group.add(mesh1);
    //把mesh2型插入到组group中，mesh2作为group的子对象
    group.add(mesh2);
    //把group插入到场景中作为场景子对象
    this.scene.add(group);

    console.log('group.children - ', group.children);

    // console.log(mesh1.getWorldPosition());
    // console.log(mesh1.getWorldPosition());

    this.layerGeometry();

    const point = new THREE.PointLight(0xffffff);
    point.position.set(50, 50, 50);
    this.scene.add(point);
    const ambient = new THREE.AmbientLight(0xdddddd);
    this.scene.add(ambient);

    // 辅助坐标系  参数250表示坐标系大小，可以根据场景大小去设置
    const axisHelper = new THREE.AxesHelper(300);
    const gridHelper = new THREE.GridHelper(100, 30, 0x2c2c2c, 0x888888);
    this.scene.add(axisHelper);
    this.scene.add(gridHelper);

    return this;
  }

  public layerGeometry() {
    // 头部网格模型和组
    const headMesh = sphereMesh(10, 0, 0, 0);
    headMesh.name = '脑壳';
    const leftEyeMesh = sphereMesh(1, 8, 5, 4, 0x00ff00);
    leftEyeMesh.name = '左眼';
    const rightEyeMesh = sphereMesh(1, 8, 5, -4, 0x00ff00);
    rightEyeMesh.name = '右眼';
    const headGroup = new THREE.Group();
    headGroup.name = '头部';
    headGroup.add(headMesh, leftEyeMesh, rightEyeMesh);
    // 身体网格模型和组
    const neckMesh = cylinderMesh(3, 10, 0, -15, 0);
    neckMesh.name = '脖子';
    const leftHand = cylinderMesh(3, 30, -15, -20, 0);
    leftHand.name = '左手';
    const rightHand = cylinderMesh(3, 30, 15, -20, 0);
    rightHand.name = '右手';
    const bodyMesh = cylinderMesh(14, 30, 0, -35, 0);
    bodyMesh.name = '腹部';
    const leftLegMesh = cylinderMesh(4, 60, 0, -80, -7);
    leftLegMesh.name = '左腿';
    const rightLegMesh = cylinderMesh(4, 60, 0, -80, 7);
    rightLegMesh.name = '右腿';
    const legGroup = new THREE.Group();
    legGroup.name = '腿';
    legGroup.add(leftLegMesh, rightLegMesh);
    const bodyGroup = new THREE.Group();
    bodyGroup.name = '身体';
    bodyGroup.add(neckMesh, leftHand, rightHand, bodyMesh, legGroup);
    // 人Group
    const personGroup = new THREE.Group();
    personGroup.name = '人';
    personGroup.add(headGroup, bodyGroup);
    personGroup.translateX(-30);
    personGroup.translateY(50);
    this.scene.add(personGroup);

    personGroup.traverse(function (obj: THREE.Object3D) {
      // 打印id属性
      console.log(obj.name, obj);
    });

    // 球体网格模型创建函数
    function sphereMesh(
      R: number,
      x: number,
      y: number,
      z: number,
      color: number = 0x0000ff,
    ) {
      const geometry = new THREE.SphereGeometry(R, 25, 25); //球体几何体
      const material = new THREE.MeshPhongMaterial({
        color: color,
      }); //材质对象Material
      const mesh = new THREE.Mesh(geometry, material); // 创建网格模型对象
      mesh.position.set(x, y, z);
      return mesh;
    }
    // 圆柱体网格模型创建函数
    function cylinderMesh(
      R: number,
      h: number,
      x: number,
      y: number,
      z: number,
      color: number = 0x0000ff,
    ) {
      const geometry = new THREE.CylinderGeometry(R, R, h, 25, 25); //球体几何体
      const material = new THREE.MeshPhongMaterial({
        color: color,
      }); //材质对象Material
      const mesh = new THREE.Mesh(geometry, material); // 创建网格模型对象
      mesh.position.set(x, y, z);
      return mesh;
    }
  }

  public createControl() {
    // 鼠标操作三维场景
    const controls = new OrbitControls(this.camera, this.renderer.domElement); //创建控件对象

    // 已经通过requestAnimationFrame(render);周期性执行render函数，没必要再通过监听鼠标事件执行render函数
    // controls.addEventListener('change', () => this.draw()); //监听鼠标、键盘事件
    // 注意开发中不要同时使用requestAnimationFrame()或controls.addEventListener('change', render)调用同一个函数，这样会冲突。

    // this.renderer.render(this.scene, this.camera);

    window.addEventListener('keyup', (e) => {
      console.log(e);
      if (e.code === 'Space') {
        controls.reset();
      }
    });

    const gui = new dat.GUI();
    this.gui = gui;

    return this;
  }

  public draw(t?: number) {
    //旋转角速度0.001弧度每毫秒
    // this.model.rotateY(0.001 * (t || 1));
    // this.model.rotateX(0.001 * (t || 1));

    this.renderer.render(this.scene, this.camera);
    return this;
  }

  public startPlay() {
    let T0 = Date.now(); // 上次时间
    const animate = (delta: number) => {
      // console.log(delta);
      // 均匀旋转
      const T1 = Date.now(); // 本次时间
      const t = T1 - T0;
      T0 = T1; // 把本次时间赋值给上次时间

      this.draw(delta);

      this.player = requestAnimationFrame(animate);
    };

    this.stopPlay();
    this.player = requestAnimationFrame(animate);
  }

  public stopPlay() {
    this.player && cancelAnimationFrame(this.player);
    return this;
  }
}
