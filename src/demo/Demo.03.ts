import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

export class Demo {
  public player: number = null;
  public scene: THREE.Scene = null;
  public camera: THREE.Camera = null;
  public renderer: THREE.Renderer = null;

  public model: THREE.Object3D = null;

  public constructor(private container: HTMLElement) {
    this.createScene().createGeometry()
    .createControl()
    .startPlay();
  }

  public static init(container: HTMLElement): Demo {
    return new Demo(container);
  }

  public createScene() {

    const width = window.innerWidth; // 窗口宽度
    const height = window.innerHeight; // 窗口高度
    const k = width / height; // 窗口宽高比
    const s = 200; // 三维场景显示范围控制系数，系数越大，显示的范围越大
    //创建相机对象

    const scene = new THREE.Scene();
    const renderer = new THREE.WebGLRenderer();
    const camera = new THREE.OrthographicCamera(-s * k, s * k, s, -s, 1, 1000);

    camera.position.set(0, 100, 300);
    camera.lookAt(scene.position);

    this.scene = scene;
    this.camera = camera;
    this.renderer = renderer;

    renderer.setSize(width, height);
    renderer.setClearColor(0xb9d3ff, 1);
    this.container.appendChild(renderer.domElement);

    return this;
  }

  public createGeometry() {

    // const geometry = new THREE.SphereGeometry(60, 40, 40);
    const geometry = new THREE.BoxGeometry(100, 10, 10);
    const material = new THREE.MeshLambertMaterial({
      color: 0x0000ff
    });
    const mesh = new THREE.Mesh(geometry, material); // 网格模型对象 Mesh

    mesh.position.set(0, 0, 0);

    this.model = mesh;
    this.scene.add(mesh);

    const point = new THREE.PointLight(0xffffff);
    point.position.set(400, 200, 300); //点光源位置
    this.scene.add(point); // 点光源添加到场景中

    const ambient = new THREE.AmbientLight(0x444444);
    this.scene.add(ambient);

    return this;
  }

  public createControl() {
    // 鼠标操作三维场景
    const controls = new OrbitControls(this.camera, this.renderer.domElement);//创建控件对象
    console.log(controls);

    // 已经通过requestAnimationFrame(render);周期性执行render函数，没必要再通过监听鼠标事件执行render函数
    // controls.addEventListener('change', () => this.draw()); //监听鼠标、键盘事件
    // 注意开发中不要同时使用requestAnimationFrame()或controls.addEventListener('change', render)调用同一个函数，这样会冲突。

    // this.renderer.render(this.scene, this.camera);
    return this;
  }

  public draw(t?: number) {

    //旋转角速度0.001弧度每毫秒
    this.model.rotateY(0.001 * (t || 1));
    this.model.rotateX(0.001 * (t || 1));

    this.renderer.render(this.scene, this.camera);
    return this;
  }

  public startPlay() {
    let T0 = Date.now();  // 上次时间
    const animate = () => {
      // 均匀旋转
      const T1 = Date.now();  // 本次时间
      const t = T1 - T0;
      T0 = T1;  // 把本次时间赋值给上次时间

      this.draw(t);

      this.player = requestAnimationFrame(animate);
    };
    this.stopPlay();
    animate();
  }

  public stopPlay() {
    this.player && cancelAnimationFrame(this.player);
    return this;
  }
}
