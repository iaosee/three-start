import * as THREE from 'three';
import * as dat from 'dat.gui';

import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

export class Demo {
  public player: number = null;
  public scene: THREE.Scene = null;
  public camera: THREE.Camera = null;
  public renderer: THREE.Renderer = null;

  public gltfModel: GLTF = null;
  public model: THREE.Object3D = null;

  // https://github.com/mrdoob/three.js/blob/dev/src/core/Clock.js
  public clock = new THREE.Clock();
  public animateActions: THREE.AnimationAction[] = [];

  public gui: dat.GUI = null;

  public constructor(private container: HTMLElement) {
    (window as any).d = this;

    this.createScene().createGeometry().createControl().startPlay();
  }

  public static init(container: HTMLElement): Demo {
    return new Demo(container);
  }

  public createScene() {
    const width = window.innerWidth; // 窗口宽度
    const height = window.innerHeight; // 窗口高度

    const scene = new THREE.Scene();
    const renderer = new THREE.WebGLRenderer({
      //增加下面两个属性，可以抗锯齿
      antialias: true,
      alpha: true,
    });

    const camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000);

    renderer.setPixelRatio(window.devicePixelRatio);
    camera.position.set(20, 30, 60);
    camera.lookAt(scene.position);

    this.scene = scene;
    this.camera = camera;
    this.renderer = renderer;

    renderer.setSize(width, height);
    renderer.setClearColor(0xb9d3ff, 1);
    this.container.appendChild(renderer.domElement);

    return this;
  }

  public createGeometry() {
    const geometry = new THREE.BufferGeometry();
    const arc = new THREE.ArcCurve(0, 0, 50, 0, 2 * Math.PI, false);
    const points = arc.getPoints(20);
    geometry.setFromPoints(points);
    const material = new THREE.LineBasicMaterial({
      color: 0x000000,
    });
    const line = new THREE.Line(geometry, material);
    this.scene.add(line);

    const geometry2 = new THREE.BufferGeometry();
    const LineCurve = new THREE.LineCurve3(
      new THREE.Vector3(50, 0, 0),
      new THREE.Vector3(0, 50, 0),
    );
    const pointArr = LineCurve.getPoints(20);
    geometry2.setFromPoints(pointArr);
    const materia2 = new THREE.LineBasicMaterial({
      color: 0x000000,
    });
    const line2 = new THREE.Line(geometry2, materia2);
    this.scene.add(line2);

    const geometry3 = new THREE.BufferGeometry();
    const curve = new THREE.CatmullRomCurve3([
      new THREE.Vector3(-50, 20, 90),
      new THREE.Vector3(-10, 40, 40),
      new THREE.Vector3(0, 0, 0),
      new THREE.Vector3(60, -60, 0),
      new THREE.Vector3(70, 0, 80),
    ]);
    geometry3.setFromPoints(curve.getPoints(20));
    const line3 = new THREE.Line(
      geometry3,
      new THREE.LineBasicMaterial({
        color: 0x000000,
        linewidth: 10,
      }),
    );
    this.scene.add(line3);

    const geometry4 = new THREE.BufferGeometry();
    const p1 = new THREE.Vector3(-80, 0, 0);
    const p2 = new THREE.Vector3(-40, 100, 0);
    const p3 = new THREE.Vector3(40, 100, 0);
    const p4 = new THREE.Vector3(80, 0, 0);
    const cubicCurve = new THREE.CubicBezierCurve3(p1, p2, p3, p4);
    geometry4.setFromPoints(cubicCurve.getPoints(20));
    const line4 = new THREE.Line(
      geometry4,
      new THREE.LineBasicMaterial({
        color: 0x000000,
        linewidth: 10,
      }),
    );
    this.scene.add(line4);

    //创建管道成型的路径(3D样条曲线)
    // const path = new THREE.LineCurve3(new THREE.Vector3(0, 100, 0), new THREE.Vector3(0, 0, 0));
    const path = new THREE.CatmullRomCurve3([
      new THREE.Vector3(-10, -50, -50),
      new THREE.Vector3(10, 0, 0),
      new THREE.Vector3(8, 50, 50),
      new THREE.Vector3(-5, 0, 100),
    ]);
    /**
     * TubeGeometry 参数
     * path - 扫描路径，基本类是Curve的路径构造函数
     * tubularSegments - 路径方向细分数，默认64
     * radius - 管道半径，默认1
     * radiusSegments - 管道圆弧细分数，默认8
     * closed - Boolean值，管道是否闭合
     */
    const geometry5 = new THREE.TubeGeometry(path, 10, 4, 10);
    const mesh5 = new THREE.Mesh(
      geometry5,
      new THREE.MeshBasicMaterial({
        color: 0x0000ff,
        side: THREE.DoubleSide,
        wireframe: true,
      }),
    );
    this.scene.add(mesh5);

    const geometry6 = new THREE.LatheGeometry(
      [
        new THREE.Vector2(20, 30),
        new THREE.Vector2(15, 0),
        new THREE.Vector2(20, -30),
      ],
      30,
    );
    const material6 = new THREE.MeshPhongMaterial({
      color: 0x0000ff,
      side: THREE.DoubleSide,
      wireframe: true,
    });
    const mesh6 = new THREE.Mesh(geometry6, material6); //旋转网格模型对象
    mesh6.translateX(-25);
    this.scene.add(mesh6);

    const shape = new THREE.Shape();
    shape.splineThru([
      new THREE.Vector2(20, 30),
      new THREE.Vector2(15, 0),
      new THREE.Vector2(20, -30),
    ]);
    const splinePoints = shape.getPoints(5); //插值计算细分数20
    const geometry7 = new THREE.LatheGeometry(splinePoints, 10); //旋转造型
    const material7 = new THREE.MeshPhongMaterial({
      color: 0x0000ff,
      side: THREE.DoubleSide,
      wireframe: true,
    });
    const mesh7 = new THREE.Mesh(geometry7, material7); //旋转网格模型对象
    mesh7.translateX(-25);
    mesh7.translateZ(50);
    this.scene.add(mesh7);

    // 一个外轮廓圆弧嵌套三个内圆弧轮廓
    const shape2 = new THREE.Shape();
    shape2.arc(0, 0, 100, 0, 2 * Math.PI, false);
    const path1 = new THREE.Path();
    path1.arc(0, 0, 40, 0, 2 * Math.PI, false);
    const path2 = new THREE.Path();
    path2.arc(80, 0, 10, 0, 2 * Math.PI, false);
    const path3 = new THREE.Path();
    path3.arc(-80, 0, 10, 0, 2 * Math.PI, false);
    shape2.holes.push(path1, path2, path3);
    const geometry8 = new THREE.ShapeGeometry(shape2, 10); //旋转造型
    const material8 = new THREE.MeshPhongMaterial({
      color: 0x0000ff,
      side: THREE.DoubleSide,
      // wireframe: true,
    });
    const mesh8 = new THREE.Mesh(geometry8, material8);
    mesh8.translateZ(-50);
    this.scene.add(mesh8);

    const shape3 = new THREE.Shape();
    /**四条直线绘制一个矩形轮廓*/
    shape3.moveTo(50, 50); //起点
    shape3.lineTo(50, 100); //第2点
    shape3.lineTo(100, 100); //第3点
    shape3.lineTo(100, 50); //第4点
    shape3.lineTo(50, 50); //第5点
    const geometry9 = new THREE.ExtrudeGeometry( //拉伸造型
      shape3, //二维轮廓
      {
        depth: 100,
        bevelEnabled: false, //无倒角
        steps: 50, //扫描方向细分数
        extrudePath: curve, //选择扫描轨迹
      },
    );
    const material9 = new THREE.MeshPhongMaterial({
      color: 0x0000ff,
      wireframe: true,
    }); //材质对象
    const mesh9 = new THREE.Mesh(geometry9, material9); //点模型对象
    this.scene.add(mesh9); //点模型添加到场景中

    const point = new THREE.PointLight(0xffffff);
    point.position.set(400, 200, 300);
    this.scene.add(point);
    const ambient = new THREE.AmbientLight(0x444444);
    this.scene.add(ambient);

    // 辅助坐标系  参数250表示坐标系大小，可以根据场景大小去设置
    const axisHelper = new THREE.AxesHelper(300);
    // const gridHelper = new THREE.GridHelper(100, 30, 0x2c2c2c, 0x888888);
    this.scene.add(axisHelper);
    // this.scene.add(gridHelper);

    return this;
  }

  public createControl() {
    // 鼠标操作三维场景
    const controls = new OrbitControls(this.camera, this.renderer.domElement); //创建控件对象

    // 已经通过requestAnimationFrame(render);周期性执行render函数，没必要再通过监听鼠标事件执行render函数
    // controls.addEventListener('change', () => this.draw()); //监听鼠标、键盘事件
    // 注意开发中不要同时使用requestAnimationFrame()或controls.addEventListener('change', render)调用同一个函数，这样会冲突。

    // this.renderer.render(this.scene, this.camera);

    window.addEventListener('keyup', (e) => {
      console.log(e);
      if (e.code === 'Space') {
        controls.reset();
      }
    });

    const gui = new dat.GUI();
    this.gui = gui;

    return this;
  }

  public draw(t?: number) {
    //旋转角速度0.001弧度每毫秒
    // this.model.rotateY(0.001 * (t || 1));
    // this.model.rotateX(0.001 * (t || 1));

    this.renderer.render(this.scene, this.camera);
    return this;
  }

  public startPlay() {
    let T0 = Date.now(); // 上次时间
    const animate = (delta: number) => {
      // console.log(delta);
      // 均匀旋转
      const T1 = Date.now(); // 本次时间
      const t = T1 - T0;
      T0 = T1; // 把本次时间赋值给上次时间

      this.draw(delta);

      this.player = requestAnimationFrame(animate);
    };

    this.stopPlay();
    this.player = requestAnimationFrame(animate);
  }

  public stopPlay() {
    this.player && cancelAnimationFrame(this.player);
    return this;
  }
}
