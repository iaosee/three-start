import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';

export class Demo {
  public player: number = null;
  public scene: THREE.Scene = null;
  public camera: THREE.Camera = null;
  public renderer: THREE.Renderer = null;

  public constructor(private container: HTMLElement) {
    this.createScene()
      .createGeometry()
      .loadModel()
      .startPlay();
  }

  public static init(container: HTMLElement): Demo {
    return new Demo(container);
  }

  public createScene() {
    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera(
      75,
      window.innerWidth / window.innerHeight,
      0.1,
      1000,
    );
    // const renderer = new THREE.WebGLRenderer();
    const canvas = document.createElement('canvas');
    const context = canvas.getContext('webgl2');
    const renderer = new THREE.WebGLRenderer({
      canvas: canvas,
      context: context,
    });

    this.scene = scene;
    this.camera = camera;
    this.renderer = renderer;

    renderer.setSize(window.innerWidth, window.innerHeight);
    this.container.appendChild(renderer.domElement);

    camera.position.set(0, 0, 100);
    camera.lookAt(0, 0, 0);

    return this;
  }

  public createGeometry() {
    const material = new THREE.LineBasicMaterial({ color: 0x00ff00 });

    const points: THREE.Vector3[] = [];
    points.push(new THREE.Vector3(-10, 0, 0));
    points.push(new THREE.Vector3(0, 10, 0));
    points.push(new THREE.Vector3(10, 0, 0));

    const geometry = new THREE.BufferGeometry().setFromPoints(points);
    const line = new THREE.Line(geometry, material);

    this.scene.add(line);

    return this;
  }

  public loadModel() {
    // https://iaosee-resource.oss-cn-beijing.aliyuncs.com/model/Soldier.glb
    const loader = new GLTFLoader();

    loader.load(
      'https://iaosee-resource.oss-cn-beijing.aliyuncs.com/model/Soldier.glb',
      (gltf) => {
        console.log(gltf);

        const obj = gltf.scene;

        obj.position.set(0, 0, 80);
        obj.scale.set(4, 4, 4);

        this.scene.add(obj);
      },
      undefined,
      (error) => {
        console.error(error);
      },
    );

    return this;
  }

  public draw() {
    this.renderer.render(this.scene, this.camera);
    return this;
  }

  public startPlay() {
    const animate = () => {
      this.draw();
      this.player = requestAnimationFrame(animate);
    };
    this.stopPlay();
    animate();
  }

  public stopPlay() {
    this.player && cancelAnimationFrame(this.player);
    return this;
  }
}
