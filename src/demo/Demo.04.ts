import * as THREE from 'three';

import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

export class Demo {
  public player: number = null;
  public scene: THREE.Scene = null;
  public camera: THREE.Camera = null;
  public renderer: THREE.Renderer = null;

  public model1: THREE.Object3D = null;
  public model2: THREE.Object3D = null;
  public model3: THREE.Object3D = null;

  public constructor(private container: HTMLElement) {
    this.createScene().createGeometry().createControl().startPlay();
  }

  public static init(container: HTMLElement): Demo {
    return new Demo(container);
  }

  public createScene() {
    const width = window.innerWidth; // 窗口宽度
    const height = window.innerHeight; // 窗口高度

    const scene = new THREE.Scene();
    const renderer = new THREE.WebGLRenderer({
      //增加下面两个属性，可以抗锯齿
      antialias: true,
      alpha: true,
    });
    const camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000);

    // console.log(renderer.getPixelRatio());
    renderer.setPixelRatio(window.devicePixelRatio);
    camera.position.set(0, 0, 200);
    camera.lookAt(scene.position);

    this.scene = scene;
    this.camera = camera;
    this.renderer = renderer;

    renderer.setSize(width, height);
    renderer.setClearColor(0xb9d3ff, 1);
    this.container.appendChild(renderer.domElement);

    return this;
  }

  public createGeometry() {
    /**
Three 中提供了很多的常见几何体的 API
  new THREE.BoxGeometry(100, 100, 100);
  new THREE.SphereGeometry(60, 40, 40);  // 球体 参数：半径60  经纬度细分数40,40
  new THREE.CylinderGeometry( 50, 50, 100, 25 ); // 圆柱  参数：圆柱面顶部、底部直径50,50   高度100  圆周分段数
  new THREE.OctahedronGeometry(50);  // 正八面体
  new THREE.DodecahedronGeometry(50);  // 正十二面体
  new THREE.IcosahedronGeometry(50);   // 正二十面体
  */

    // 立方体网格模型
    const geometry1 = new THREE.BoxGeometry(60, 60, 60);
    const material1 = new THREE.MeshLambertMaterial({
      color: 0xff0000,
      wireframe: true, // 将几何图形渲染为线框。 默认值为false
    }); //材质对象Material
    const mesh1 = new THREE.Mesh(geometry1, material1); //网格模型对象Mesh
    mesh1.position.set(-120, 0, 0);
    this.scene.add(mesh1); //网格模型添加到场景中

    /**
ThreeJS 提供了很多常用的材质效果
  MeshBasicMaterial - 基础网格材质，不受光照影响的材质
  MeshLambertMaterial - Lambert网格材质，与光照有反应，漫反射
  MeshPhongMaterial - 高光Phong材质,与光照有反应
  MeshStandardMaterial - PBR物理材质，相比较高光Phong材质可以更好的模拟金属、玻璃等效果
 */

    // 球体网格模型
    const geometry2 = new THREE.SphereGeometry(60, 40, 40);
    const material2 = new THREE.MeshPhongMaterial({
      color: 0x00ff00,
      specular: 0x4488ee,
      shininess: 12,
    });
    const mesh2 = new THREE.Mesh(geometry2, material2); // 网格模型对象Mesh
    this.scene.add(mesh2);

    // 圆柱网格模型
    const geometry3 = new THREE.CylinderGeometry(30, 30, 100, 25);
    const material3 = new THREE.MeshLambertMaterial({
      color: 0xffff00,
      opacity: 0.7,
      transparent: true,
    });
    var mesh3 = new THREE.Mesh(geometry3, material3); //网格模型对象Mesh
    // mesh3.translateX(120); //球体网格模型沿Y轴正方向平移120
    mesh3.position.set(120, 0, 0); //设置mesh3模型对象的xyz坐标为120,0,0
    this.scene.add(mesh3); //

    this.model1 = mesh1;
    this.model2 = mesh2;
    this.model3 = mesh3;

    /**
ThreeJS 提供了生活中常见的一些光源的 API
  AmbientLight - 环境光
  PointLight - 点光源
  DirectionalLight - 平行光，比如太阳光
  SpotLight - 聚光源
 */

    const point = new THREE.PointLight(0xffffff);
    point.position.set(400, 200, 300); //点光源位置
    this.scene.add(point); // 点光源添加到场景中
    const ambient = new THREE.AmbientLight(0x444444);
    this.scene.add(ambient);

    const point2 = new THREE.PointLight(0xffffff);
    point2.position.set(-400, -200, -300); // 点光源位置
    this.scene.add(point2); // 点光源添加到场景中

    // 辅助坐标系  参数250表示坐标系大小，可以根据场景大小去设置
    const axisHelper = new THREE.AxesHelper(300);
    this.scene.add(axisHelper);

    return this;
  }

  public createControl() {
    // 鼠标操作三维场景
    const controls = new OrbitControls(this.camera, this.renderer.domElement); //创建控件对象
    console.log(controls);

    // 已经通过requestAnimationFrame(render);周期性执行render函数，没必要再通过监听鼠标事件执行render函数
    // controls.addEventListener('change', () => this.draw()); //监听鼠标、键盘事件
    // 注意开发中不要同时使用requestAnimationFrame()或controls.addEventListener('change', render)调用同一个函数，这样会冲突。
    // this.renderer.render(this.scene, this.camera);

    window.addEventListener('keyup', (e) => {
      if (e.code === 'Space') {
        controls.reset();
      }
    });
    return this;
  }

  public draw(t?: number) {
    //旋转角速度0.001弧度每毫秒
    this.model1.rotateY(0.001 * (t || 1));
    this.model1.rotateX(0.001 * (t || 1));
    this.model2.rotateY(0.001 * (t || 1));
    this.model2.rotateX(0.001 * (t || 1));
    this.model3.rotateY(0.001 * (t || 1));
    this.model3.rotateX(0.001 * (t || 1));

    this.renderer.render(this.scene, this.camera);
    return this;
  }

  public startPlay() {
    let T0 = Date.now(); // 上次时间
    const animate = () => {
      // 均匀旋转
      const T1 = Date.now(); // 本次时间
      const t = T1 - T0;
      T0 = T1; // 把本次时间赋值给上次时间

      this.draw(t);

      this.player = requestAnimationFrame(animate);
    };
    this.stopPlay();
    animate();
  }

  public stopPlay() {
    this.player && cancelAnimationFrame(this.player);
    return this;
  }
}
