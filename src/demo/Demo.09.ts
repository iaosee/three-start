import * as THREE from 'three';
import * as dat from 'dat.gui';

import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

export class Demo {
  public player: number = null;
  public scene: THREE.Scene = null;
  public camera: THREE.Camera = null;
  public renderer: THREE.Renderer = null;

  public gltfModel: GLTF = null;
  public model: THREE.Object3D = null;

  // https://github.com/mrdoob/three.js/blob/dev/src/core/Clock.js
  public clock = new THREE.Clock();
  public animateActions: THREE.AnimationAction[] = [];

  public gui: dat.GUI = null;

  public constructor(private container: HTMLElement) {
    (window as any).d = this;

    this.createScene().createGeometry().createControl().startPlay();
  }

  public static init(container: HTMLElement): Demo {
    return new Demo(container);
  }

  public createScene() {
    const width = window.innerWidth; // 窗口宽度
    const height = window.innerHeight; // 窗口高度

    const scene = new THREE.Scene();
    const renderer = new THREE.WebGLRenderer({
      //增加下面两个属性，可以抗锯齿
      antialias: true,
      alpha: true,
    });

    const camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000);

    renderer.setPixelRatio(window.devicePixelRatio);
    camera.position.set(20, 30, 60);
    camera.lookAt(scene.position);

    this.scene = scene;
    this.camera = camera;
    this.renderer = renderer;

    renderer.setSize(width, height);
    renderer.setClearColor(0xb9d3ff, 1);
    this.container.appendChild(renderer.domElement);

    return this;
  }

  public createGeometry() {
    const geometry = new THREE.BoxGeometry(40, 100, 40);
    const material = new THREE.MeshLambertMaterial({
      color: 0x0000ff,
    });
    const mesh = new THREE.Mesh(geometry, material); //网格模型对象Mesh
    mesh.position.set(0, 0, 0);
    // 设置产生投影的网格模型
    mesh.castShadow = true;
    this.scene.add(mesh); //网格模型添加到场景中

    const planeGeometry = new THREE.PlaneGeometry(300, 200);
    const planeMaterial = new THREE.MeshLambertMaterial({
      color: 0x999999,
    });
    const planeMesh = new THREE.Mesh(planeGeometry, planeMaterial);
    planeMesh.rotateX(-Math.PI / 2);
    planeMesh.position.y = -50;
    // 设置接收阴影的投影面
    planeMesh.receiveShadow = true;
    this.scene.add(planeMesh);

    const directionalLight = new THREE.DirectionalLight(0xffffff, 1);
    directionalLight.position.set(60, 100, 40);
    this.scene.add(directionalLight);
    // 设置用于计算阴影的光源对象
    directionalLight.castShadow = true;
    // 设置计算阴影的区域，最好刚好紧密包围在对象周围
    // 计算阴影的区域过大：模糊  过小：看不到或显示不完整
    directionalLight.shadow.camera.near = 0.5;
    directionalLight.shadow.camera.far = 300;
    directionalLight.shadow.camera.left = -50;
    directionalLight.shadow.camera.right = 50;
    directionalLight.shadow.camera.top = 200;
    directionalLight.shadow.camera.bottom = -100;
    console.log(directionalLight.shadow.camera);

    const spotLight = new THREE.SpotLight(0xffffff);
    // 设置聚光光源位置
    spotLight.position.set(-30, 120, 50);
    // 设置聚光光源发散角度
    spotLight.angle = Math.PI / 6;
    this.scene.add(spotLight); //光对象添加到scene场景中
    // 设置用于计算阴影的光源对象
    spotLight.castShadow = true;
    // 设置计算阴影的区域，注意包裹对象的周围
    spotLight.shadow.camera.near = 1;
    spotLight.shadow.camera.far = 300;
    spotLight.shadow.camera.fov = 20;

    // const point = new THREE.PointLight(0xffffff);
    // point.position.set(50, 50, 50);
    // this.scene.add(point);
    // const ambient = new THREE.AmbientLight(0xdddddd);
    // this.scene.add(ambient);

    // 辅助坐标系  参数250表示坐标系大小，可以根据场景大小去设置
    const axisHelper = new THREE.AxesHelper(300);
    const gridHelper = new THREE.GridHelper(100, 30, 0x2c2c2c, 0x888888);
    this.scene.add(axisHelper);
    this.scene.add(gridHelper);

    return this;
  }

  public createControl() {
    // 鼠标操作三维场景
    const controls = new OrbitControls(this.camera, this.renderer.domElement); //创建控件对象

    // 已经通过requestAnimationFrame(render);周期性执行render函数，没必要再通过监听鼠标事件执行render函数
    // controls.addEventListener('change', () => this.draw()); //监听鼠标、键盘事件
    // 注意开发中不要同时使用requestAnimationFrame()或controls.addEventListener('change', render)调用同一个函数，这样会冲突。

    // this.renderer.render(this.scene, this.camera);
    window.addEventListener('keyup', (e) => {
      if (e.code === 'Space') {
        controls.reset();
      }
    });

    const gui = new dat.GUI();
    this.gui = gui;

    return this;
  }

  public draw(t?: number) {
    //旋转角速度0.001弧度每毫秒
    // this.model.rotateY(0.001 * (t || 1));
    // this.model.rotateX(0.001 * (t || 1));

    this.renderer.render(this.scene, this.camera);
    return this;
  }

  public startPlay() {
    let T0 = Date.now(); // 上次时间
    const animate = (delta: number) => {
      // console.log(delta);
      // 均匀旋转
      const T1 = Date.now(); // 本次时间
      const t = T1 - T0;
      T0 = T1; // 把本次时间赋值给上次时间

      this.draw(delta);

      this.player = requestAnimationFrame(animate);
    };

    this.stopPlay();
    this.player = requestAnimationFrame(animate);
  }

  public stopPlay() {
    this.player && cancelAnimationFrame(this.player);
    return this;
  }
}
