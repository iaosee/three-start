const routes = [
  { path: '/', component: '@/pages/Home' },
  { path: '/demo-01', component: '@/pages/Page01' },
  { path: '/demo-02', component: '@/pages/Page02' },
  { path: '/demo-03', component: '@/pages/Page03' },
  { path: '/demo-04', component: '@/pages/Page04' },
  { path: '/demo-05', component: '@/pages/Page05' },
  { path: '/demo-06', component: '@/pages/Page06' },
  { path: '/demo-07', component: '@/pages/Page07' },
  { path: '/demo-08', component: '@/pages/Page08' },
  { path: '/demo-09', component: '@/pages/Page09' },
  { path: '/demo-10', component: '@/pages/Page10' },
  { path: '/demo-11', component: '@/pages/Page11' },
  { path: '/demo-12', component: '@/pages/Page12' },
  { path: '/demo-13', component: '@/pages/Page13' },
];

export default routes;
