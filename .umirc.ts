import { defineConfig } from 'umi';
import routes from './src/config/routes';

console.log(routes);

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes: routes,
  fastRefresh: {},
  devServer: {
    https: true,
  },
});
